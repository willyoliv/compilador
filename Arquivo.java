
package compilador;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

public class Arquivo {
        public String conteudo = "";
	public String read(String caminho) throws UnsupportedEncodingException{
        try {
            File arquivo = new File(caminho);
            BufferedReader ler_arquivo = new BufferedReader(new InputStreamReader(new FileInputStream(arquivo),"UTF-8"));
            String linha="";
            try {
                linha = ler_arquivo.readLine();
                while(linha!=null){
                    this.conteudo += linha+"\n";
                    linha = ler_arquivo.readLine();
                }
                ler_arquivo.close();
                return this.conteudo;
            } catch (IOException ex) {
                return "";
            }
        } catch (FileNotFoundException ex) {
            return "";
        }
    }
}
