package compilador;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;

public final class Compilador {

    private ArrayList<String> simboloReconhecido = new ArrayList<>();
    private ArrayList<String> classificacaoSimbolo = new ArrayList<>();
    private final ArrayList<String> palavras_reservadas = new ArrayList<>(Arrays.asList(new String[]{"se", "entao", "faca", "else", "enquanto"}));
    private String token = "";
    private int estado = 0, i = 0;
    private String texto = "";
    private String letras[] = {""};

    public Compilador() throws UnsupportedEncodingException {
        String arq = "arq.txt";
        Arquivo arquivo = new Arquivo();
        this.texto = arquivo.read(arq);
        this.letras = texto.split("");
        this.separadorLexico();
        this.printTokens();
    }

    public void separadorLexico() {
        while (i < this.letras.length) {
            switch (estado) {
                case 0:
                    switch (letras[i]) {
                        //letras 
                        case "a": estado = 1; token += "a"; break;
                        case "b": estado = 1; token += "b"; break;
                        case "c": estado = 1; token += "c"; break;
                        case "d": estado = 1; token += "d"; break;
                        case "e": estado = 1; token += "e"; break;
                        case "f": estado = 1; token += "f"; break;
                        case "g": estado = 1; token += "g"; break;
                        case "h": estado = 1; token += "h"; break;
                        case "i": estado = 1; token += "i"; break;
                        case "j": estado = 1; token += "j"; break;
                        case "k": estado = 1; token += "k"; break;
                        case "l": estado = 1; token += "l"; break;
                        case "m": estado = 1; token += "m"; break;
                        case "n": estado = 1; token += "n"; break;
                        case "o": estado = 1; token += "o"; break;
                        case "p": estado = 1; token += "p"; break;
                        case "q": estado = 1; token += "q"; break;
                        case "r": estado = 1; token += "r"; break;
                        case "s": estado = 1; token += "s"; break;
                        case "t": estado = 1; token += "t"; break;
                        case "u": estado = 1; token += "u"; break;
                        case "v": estado = 1; token += "v"; break;
                        case "w": estado = 1; token += "w"; break;
                        case "x": estado = 1; token += "x"; break;
                        case "y": estado = 1; token += "y"; break;
                        case "z": estado = 1; token += "z"; break;
                        case "A": estado = 1; token += "A"; break;
                        case "B": estado = 1; token += "B"; break;
                        case "C": estado = 1; token += "C"; break;
                        case "D": estado = 1; token += "D"; break;
                        case "E": estado = 1; token += "E"; break;
                        case "F": estado = 1; token += "F"; break;
                        case "G": estado = 1; token += "G"; break;
                        case "H": estado = 1; token += "H"; break;
                        case "I": estado = 1; token += "I"; break;
                        case "J": estado = 1; token += "J"; break;
                        case "K": estado = 1; token += "K"; break;
                        case "L": estado = 1; token += "L"; break;
                        case "M": estado = 1; token += "M"; break;
                        case "N": estado = 1; token += "N"; break;
                        case "O": estado = 1; token += "O"; break;
                        case "P": estado = 1; token += "P"; break;
                        case "Q": estado = 1; token += "Q"; break;
                        case "R": estado = 1; token += "R"; break;
                        case "S": estado = 1; token += "S"; break;
                        case "T": estado = 1; token += "T"; break;
                        case "U": estado = 1; token += "U"; break;
                        case "V": estado = 1; token += "V"; break;
                        case "W": estado = 1; token += "W"; break;
                        case "X": estado = 1; token += "X"; break;
                        case "Y": estado = 1; token += "Y"; break;
                        case "Z": estado = 1; token += "Z"; break;
                        //digitos
                        case "-": estado = 3; token += "-"; break;
                        case "0": estado = 2; token += "0"; break;
                        case "1": estado = 2; token += "1"; break;
                        case "2": estado = 2; token += "2"; break;
                        case "3": estado = 2; token += "3"; break;
                        case "4": estado = 2; token += "4"; break;
                        case "5": estado = 2; token += "5"; break;
                        case "6": estado = 2; token += "6"; break;
                        case "7": estado = 2; token += "7"; break;
                        case "8": estado = 2; token += "8"; break;
                        case "9": estado = 2; token += "9"; break;
                        //simbolos especiais
                        case ".":
                            estado = 0;
                            this.simboloReconhecido.add(letras[i]);                  
                            this.classificacaoSimbolo.add("simbolo especial");
                            token = "";
                            break;
                        case ",":
                            estado = 0;
                            this.simboloReconhecido.add(letras[i]);                  
                            this.classificacaoSimbolo.add("simbolo especial");
                            token = "";
                            break;
                        case ";":
                            estado = 0;
                            this.simboloReconhecido.add(letras[i]);                  
                            this.classificacaoSimbolo.add("simbolo especial");
                            token = "";
                            break;
                        case "+":
                            estado = 0;
                            this.simboloReconhecido.add(letras[i]);                  
                            this.classificacaoSimbolo.add("simbolo especial");
                            token = "";
                            break;
                        case "(":
                            estado = 0;
                            this.simboloReconhecido.add(letras[i]);                  
                            this.classificacaoSimbolo.add("simbolo especial");
                            token = "";
                            break;
                        case "{":
                            estado = 0;
                            this.simboloReconhecido.add(letras[i]);                  
                            this.classificacaoSimbolo.add("simbolo especial");
                            token = "";
                            break;
                        case "}":
                            estado = 0;
                            this.simboloReconhecido.add(letras[i]);                  
                            this.classificacaoSimbolo.add("simbolo especial");
                            token = "";
                            break;
                        case ")":
                            estado = 0;
                            this.simboloReconhecido.add(letras[i]);                  
                            this.classificacaoSimbolo.add("simbolo especial");
                            token = "";
                            break;
                        case "/":
                            if (i != letras.length - 1) {
                                if (letras[i + 1].equals("/") && i + 2 == letras.length) {
                                    this.simboloReconhecido.add("erro");                  
                                    this.classificacaoSimbolo.add("Má formação de token comentário");
                                }
                                if (letras[i + 1].equals("/")) {
                                    estado = 7;
                                    token += "/" + letras[i + 1];
                                    i++;
                                    break;
                                } else {
                                    estado = 0;
                                    this.simboloReconhecido.add(letras[i]);                  
                                    this.classificacaoSimbolo.add("simbolo especial");
                                    token = "";
                                    break;
                                }
                            }
                        case "<":
                            if (letras.length - 1 != i) {
                                //trata iniciar um comentário quando não existe mais simbolos para ler
                                if (letras[i + 1].equals("#") && i + 2 == letras.length) {
                                    //System.out.println("Má formação de token");
                                    this.simboloReconhecido.add("erro");                  
                                    this.classificacaoSimbolo.add("Má formação de token");
                                }
                                //trata o "<="
                                if (letras[i + 1].equals("=")) {
                                    estado = 0;
                                    token += "<" + letras[i + 1];
                                    i++;
                                    this.simboloReconhecido.add(token);                  
                                    this.classificacaoSimbolo.add("simbolo especial");
                                    token = "";
                                    break;
                                    //trata o "<>"
                                } else if (letras[i + 1].equals(">")) {
                                    estado = 0;
                                    token += "<" + letras[i + 1];
                                    i++;
                                    this.simboloReconhecido.add(token);                  
                                    this.classificacaoSimbolo.add("simbolo especial");
                                    token = "";
                                    break;
                                    //trata o comentário "<# #>"
                                } else if (letras[i + 1].equals("#")) {
                                    estado = 8;
                                    token += "<" + letras[i + 1];
                                    i++;
                                    break;
                                }
                            }
                            estado = 0;
                            this.simboloReconhecido.add(letras[i]);                  
                            this.classificacaoSimbolo.add("simbolo especial");
                            token = "";
                            break;
                        case ">":
                            if (i != letras.length - 1) {
                                if (letras[i + 1].equals("=")) {
                                    estado = 0;
                                    token += ">" + letras[i + 1];
                                    this.simboloReconhecido.add(token);                  
                                    this.classificacaoSimbolo.add("simbolo especial");
                                    token = "";
                                    i++;
                                    break;
                                }
                            }
                            estado = 0;
                            this.simboloReconhecido.add(token);                  
                            this.classificacaoSimbolo.add("simbolo especial");
                            token = "";
                            break;
                        case "=":
                            estado = 0;
                            this.simboloReconhecido.add(letras[i]);                  
                            this.classificacaoSimbolo.add("simbolo especial");
                            token = "";
                            break;
                        case ":":
                            if (i != letras.length - 1) {
                                if (letras[i + 1].equals("=")) {
                                    estado = 0;
                                    token += ":" + letras[i + 1];
                                    this.simboloReconhecido.add(token);                  
                                    this.classificacaoSimbolo.add("simbolo especial");
                                    token = "";
                                    i++;
                                    break;
                                }
                            }
                            estado = 0;
                            this.simboloReconhecido.add(letras[i]);                  
                            this.classificacaoSimbolo.add("simbolo especial");
                            token = "";
                            break;
                        case "*":
                            if (i != letras.length - 1) {
                                if (letras[i + 1].equals("=")) {
                                    estado = 0;
                                    token += "*" + letras[i + 1];
                                    this.simboloReconhecido.add(token);                  
                                    this.classificacaoSimbolo.add("simbolo especial");
                                    token = "";
                                    i++;
                                    break;
                                } else if (letras[i + 1].equals("*")) {
                                    estado = 0;
                                    token += "*" + letras[i + 1];
                                    this.simboloReconhecido.add(token);                  
                                    this.classificacaoSimbolo.add("simbolo especial");
                                    token = "";
                                    i++;
                                    break;
                                }
                            }
                            estado = 0;
                            this.simboloReconhecido.add(letras[i]);                  
                            this.classificacaoSimbolo.add("simbolo especial");
                            token = "";
                            break;
                        case "@":
                            estado = 0;
                            this.simboloReconhecido.add(letras[i]);                  
                            this.classificacaoSimbolo.add("simbolo especial");
                            token = "";
                            break;//tratar comentario
                        case " ":
                            estado = 0;
                            break;
                        case "\n":
                            estado = 0;
                            break;
                        default:
                            System.out.println(letras[i]);
                            i = letras.length;
                            System.out.println("Ta errado");
                            break;
                    }
                    break;
                // parte para reconhecimento de letras
                case 1:
                    switch (letras[i]) {
                        case "a": estado = 1; token += "a"; break;
                        case "b": estado = 1; token += "b"; break;
                        case "c": estado = 1; token += "c"; break;
                        case "d": estado = 1; token += "d"; break;
                        case "e": estado = 1; token += "e"; break;
                        case "f": estado = 1; token += "f"; break;
                        case "g": estado = 1; token += "g"; break;
                        case "h": estado = 1; token += "h"; break;
                        case "i": estado = 1; token += "i"; break;
                        case "j": estado = 1; token += "j"; break;
                        case "k": estado = 1; token += "k"; break;
                        case "l": estado = 1; token += "l"; break;
                        case "m": estado = 1; token += "m"; break;
                        case "n": estado = 1; token += "n"; break;
                        case "o": estado = 1; token += "o"; break;
                        case "p": estado = 1; token += "p"; break;
                        case "q": estado = 1; token += "q"; break;
                        case "r": estado = 1; token += "r"; break;
                        case "s": estado = 1; token += "s"; break;
                        case "t": estado = 1; token += "t"; break;
                        case "u": estado = 1; token += "u"; break;
                        case "v": estado = 1; token += "v"; break;
                        case "w": estado = 1; token += "w"; break;
                        case "x": estado = 1; token += "x"; break;
                        case "y": estado = 1; token += "y"; break;
                        case "z": estado = 1; token += "z"; break;
                        case "A": estado = 1; token += "A"; break;
                        case "B": estado = 1; token += "B"; break;
                        case "C": estado = 1; token += "C"; break;
                        case "D": estado = 1; token += "D"; break;
                        case "E": estado = 1; token += "E"; break;
                        case "F": estado = 1; token += "F"; break;
                        case "G": estado = 1; token += "G"; break;
                        case "H": estado = 1; token += "H"; break;
                        case "I": estado = 1; token += "I"; break;
                        case "J": estado = 1; token += "J"; break;
                        case "K": estado = 1; token += "K"; break;
                        case "L": estado = 1; token += "L"; break;
                        case "M": estado = 1; token += "M"; break;
                        case "N": estado = 1; token += "N"; break;
                        case "O": estado = 1; token += "O"; break;
                        case "P": estado = 1; token += "P"; break;
                        case "Q": estado = 1; token += "Q"; break;
                        case "R": estado = 1; token += "R"; break;
                        case "S": estado = 1; token += "S"; break;
                        case "T": estado = 1; token += "T"; break;
                        case "U": estado = 1; token += "U"; break;
                        case "V": estado = 1; token += "V"; break;
                        case "W": estado = 1; token += "W"; break;
                        case "X": estado = 1; token += "X"; break;
                        case "Y": estado = 1; token += "Y"; break;
                        case "Z": estado = 1; token += "Z"; break;
                        case "0": estado = 1; token += "0"; break;
                        case "1": estado = 1; token += "1"; break;
                        case "2": estado = 1; token += "2"; break;
                        case "3": estado = 1; token += "3"; break;
                        case "4": estado = 1; token += "4"; break;
                        case "5": estado = 1; token += "5"; break;
                        case "6": estado = 1; token += "6"; break;
                        case "7": estado = 1; token += "7"; break;
                        case "8": estado = 1; token += "8"; break;
                        case "9": estado = 1; token += "9"; break;
                        case "#":
                            //if (i != letras.length - 1) {
                                //if(letras[i+1].equals(" ") || letras[i+1].equals("\n")){
                                    token += "#";
                                    this.simboloReconhecido.add(token);                  
                                    this.classificacaoSimbolo.add("identificador");
                                    token = "";
                                    estado = 0;
                                    break;
                              /*  }else{
                                    this.simboloReconhecido.add("erro");                  
                                    this.classificacaoSimbolo.add("má formação de token identificador");
                                    i = letras.length;
                                    break;
                                }
                            }*/
                        default:
                            estado = 0;
                            this.simboloReconhecido.add(token);                  
                            this.classificacaoSimbolo.add("identificador");
                            i--;
                            token = "";
                            break;
                    }
                    break;
                //parte para reconhecimento de digitos
                case 2:
                    switch (letras[i]) {
                        case "0": estado = 2; token += "0"; break;
                        case "1": estado = 2; token += "1"; break;
                        case "2": estado = 2; token += "2"; break;
                        case "3": estado = 2; token += "3"; break;
                        case "4": estado = 2; token += "4"; break;
                        case "5": estado = 2; token += "5"; break;
                        case "6": estado = 2; token += "6"; break;
                        case "7": estado = 2; token += "7"; break;
                        case "8": estado = 2; token += "8"; break;
                        case "9": estado = 2; token += "9"; break;
                        case ",":
                            estado = 4;
                            token += ",";
                            /*if (i == letras.length - 1) {
                                i--;
                            }*/
                            break;
                        default:
                            //if(letras[i].equals(" ") || letras[i].equals("\n")){
                                this.simboloReconhecido.add(token);                  
                                this.classificacaoSimbolo.add("digito");
                                token = "";
                                estado = 0;
                                i--;
                                break;
                           /* }else{
                                this.simboloReconhecido.add("erro");                  
                                this.classificacaoSimbolo.add("má formação de token digito");
                                i = letras.length;
                                break;
                            }*/
                    }
                    break;
                case 3:
                    switch (letras[i]) {
                        case "0": estado = 2; token += "0"; break;
                        case "1": estado = 2; token += "1"; break;
                        case "2": estado = 2; token += "2"; break;
                        case "3": estado = 2; token += "3"; break;
                        case "4": estado = 2; token += "4"; break;
                        case "5": estado = 2; token += "5"; break;
                        case "6": estado = 2; token += "6"; break;
                        case "7": estado = 2; token += "7"; break;
                        case "8": estado = 2; token += "8"; break;
                        case "9": estado = 2; token += "9"; break;
                        default:
                            estado = 0;
                            this.simboloReconhecido.add(letras[i-1]);
                            this.classificacaoSimbolo.add("simbolo especial");
                            token = "";
                            estado = 0;
                            i--;
                            break;
                    }
                    break;
                case 4:
                    switch (letras[i]) {
                        case "0": estado = 5; token += "0"; break;
                        case "1": estado = 5; token += "1"; break;
                        case "2": estado = 5; token += "2"; break;
                        case "3": estado = 5; token += "3"; break;
                        case "4": estado = 5; token += "4"; break;
                        case "5": estado = 5; token += "5"; break;
                        case "6": estado = 5; token += "6"; break;
                        case "7": estado = 5; token += "7"; break;
                        case "8": estado = 5; token += "8"; break;
                        case "9": estado = 5; token += "9"; break;
                        default:
                            this.simboloReconhecido.add("erro");                  
                            this.classificacaoSimbolo.add("má formação de token dígito");
                            i = letras.length;
                        break;
                            
                    }
                    break;
                case 5:
                    switch (letras[i]) {
                        case "0": estado = 5; token += "0"; break;
                        case "1": estado = 5; token += "1"; break;
                        case "2": estado = 5; token += "2"; break;
                        case "3": estado = 5; token += "3"; break;
                        case "4": estado = 5; token += "4"; break;
                        case "5": estado = 5; token += "5"; break;
                        case "6": estado = 5; token += "6"; break;
                        case "7": estado = 5; token += "7"; break;
                        case "8": estado = 5; token += "8"; break;
                        case "9": estado = 5; token += "9"; break;
                        default:
                            //if(letras[i].equals(" ")){
                                this.simboloReconhecido.add(token);                  
                                this.classificacaoSimbolo.add("digito");
                                token = "";
                                estado = 0;
                                i--;
                                break;
                                //corrigir o erro de 1231:
                            /*}else{
                                this.simboloReconhecido.add("erro");                  
                                this.classificacaoSimbolo.add("má formação de token digito");
                                i = letras.length;
                                break;
                            }*/
                    }
                    break;
                case 6:
                    switch (letras[i]) {

                    }
                case 7:
                    switch (letras[i]) {
                        case "/":
                            if (i != letras.length - 1) {
                                if (letras[i + 1].equals("/")) {
                                    estado = 0;
                                    token += "/" + letras[i + 1];
                                    //System.out.println(token);
                                    this.simboloReconhecido.add(token);                  
                                    this.classificacaoSimbolo.add("comentario");
                                    token = "";
                                    i++;
                                    break;
                                } else {
                                    estado = 7;
                                    token += "/";
                                    break;
                                }
                            }
                        default:
                            if (i == letras.length - 1) {
                                this.simboloReconhecido.add("erro");                  
                                this.classificacaoSimbolo.add("má formação de token comentário");
                                //System.out.println("Má formação de token, comentário");
                                break;
                            } else {
                                token += letras[i];
                                break;
                            }
                    }
                    break;
                case 8:
                    switch (letras[i]) {
                        case "#":
                            if (i != letras.length - 1) {
                                if (letras[i + 1].equals(">")) {
                                    estado = 0;
                                    token += "#" + letras[i + 1];
                                    //System.out.println(token);
                                    this.simboloReconhecido.add(token);                  
                                    this.classificacaoSimbolo.add("comentario");;
                                    token = "";
                                    i++;
                                    break;
                                } else {
                                    estado = 8;
                                    token += "#";
                                    break;
                                }
                            }
                        default:
                            if (i == letras.length - 1) {
                                this.simboloReconhecido.add("erro");                  
                                this.classificacaoSimbolo.add("má formação de token comentário");
                                break;
                            } else {
                                token += letras[i];
                                break;
                            }
                    }
                    break;
            }
            i++;
        }
    }
    public ArrayList<String> getSimboloReconhecido(){
        return this.simboloReconhecido;
    }
    public void printTokens(){
        for(int i = 0;i<simboloReconhecido.size(); i++){
            System.out.println("Simbolo: " + "'" + simboloReconhecido.get(i) + "'" + " Classificação: " + "'" +  classificacaoSimbolo.get(i) + "'");
        }
    }
}
